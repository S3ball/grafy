﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ProjektGrafy.Converters
{
    public class MarginConverter : IMultiValueConverter
    {

        //return new Thickness(System.Convert.ToDouble(value) / 2 - 4, System.Convert.ToDouble(value) / 2 - 12, 0, 0);


        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double PointSize = System.Convert.ToDouble(values[0].ToString());
            double Width = System.Convert.ToDouble(values[1].ToString());
            double Height = System.Convert.ToDouble(values[2].ToString());
            return new Thickness(PointSize / 2 - Width / 2, PointSize / 2 - Height / 2, 0, 0);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
