﻿using ProjektGrafy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ProjektGrafy.Converters
{
    class EdgeToCenterMargin : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Edge edge = values[0] as Edge;
            double PointSize = System.Convert.ToDouble(values[1].ToString());

            double centerX = (edge.Begin.X + edge.End.X) / 2 + EdgeToCenterMargin.SpanX;
            double centerY = (edge.Begin.Y + edge.End.Y) / 2 + EdgeToCenterMargin.SpanY;
            return new Thickness(centerX + PointSize / 2, centerY + PointSize / 2, 0, 0);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        public const double SpanX = 15;
        public const double SpanY = 0;
    }
}
