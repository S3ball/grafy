﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;
using ProjektGrafy.Model;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Windows;
using System.Windows.Media;
using System.Threading;
using System.Windows.Controls;


namespace ProjektGrafy.ViewModel
{
    class GraphViewModel : INotifyPropertyChanged
    {
        Graph _graph = new Graph();

        private int _windowWidth;
        public int WindowWidth
        {
            get
            {
                return _windowWidth;
            }
            set
            {
                _windowWidth = value;
                RaisePropertyChanged("WindowWidth");
                SetEdgesFunc(new object());
            }
        }

        private int _windowHeight;
        public int WindowHeight
        {
            get
            {
                return _windowHeight;
            }
            set
            {
                _windowHeight = value - 80;
                RaisePropertyChanged("WindowHeight");
                SetEdgesFunc(new object());
            }
        }

        private bool _isCohesion = false;

        private int _NumOfNodes;

        public int NumOfNodes
        {
            get { return _NumOfNodes; }
            set { _NumOfNodes = value; RaisePropertyChanged("NumOfNodes"); Message = "Podaj ilość krawędzi"; }
        }



        public bool IsCohesion
        {
            get
            {
                return _isCohesion;
            }
            set
            {
                _isCohesion = value;
            }
        }

        private int _NumOfEdges;

        public int NumOfEdges
        {
            get { return _NumOfEdges; }
            set { _NumOfEdges = value; RaisePropertyChanged("NumOfEdges"); Message = "Kliknij Generuj"; }
        }

        private int _ilWierszy;

        public int IloscWierszy
        {
            get { return _ilWierszy; }
            set { _ilWierszy = value; RaisePropertyChanged("IloscWierszy"); Message = "Kliknij Generuj"; }
        }



        public Graph MyGraph { get { return _graph; } }

        int _pointSize = 25;

        string _test;
        public string Test
        {
            get
            { return _test; }
            set
            {
                _test = value;
                RaisePropertyChanged("Test");
            }
        }

        public int PointSize
        {
            get
            {
                return _pointSize;
            }
            set
            {
                _pointSize = value;
                RaisePropertyChanged("PointSize");
                SetEdgesFunc(new object());
                Update();
            }
        }

        private string _Message;

        public string Message
        {
            get { return _Message; }
            set { _Message = value; RaisePropertyChanged("Message"); }
        }

        private Brush _ColorLow1;

        public Brush ColorLow1
        {
            get { return _ColorLow1; }
            set { _ColorLow1 = value; RaisePropertyChanged("ColorLow1"); }
        }

        private Brush _ColorLow2;

        public Brush ColorLow2
        {
            get { return _ColorLow2; }
            set { _ColorLow2 = value; RaisePropertyChanged("ColorLow2"); }
        }
        private Brush _ColorHeight;

        public Brush ColorHeight
        {
            get { return _ColorHeight; }
            set { _ColorHeight = value; RaisePropertyChanged("ColorHeight"); }
        }



        private Brush _ColorBackground;

        public Brush ColorBackground
        {
            get { return _ColorBackground; }
            set { _ColorBackground = value; RaisePropertyChanged("ColorBackground"); }
        }




        public ObservableCollection<Node> Nodes { get; set; }
        public ObservableCollection<Edge> Edges { get; set; }
        public ObservableCollection<Edge> ShortestPathEdges { get; set; }

        private GraphPath _ShortestPath = null;
        private Node _Begin = null;
        private Node _End = null;

        public GraphPath ShortestPath { get { return _ShortestPath; } set { _ShortestPath = value; RaisePropertyChanged("ShortestPath"); } }
        public Node Begin { get { return _Begin; } set { _Begin = value; RaisePropertyChanged("Begin"); } }
        public Node End { get { return _End; } set { _End = value; RaisePropertyChanged("End"); } }
        public Brush[] Lista =
        {
            Brushes.Red,
            Brushes.Green,
            Brushes.Yellow, 
            Brushes.Turquoise,
            Brushes.Black
        };


        private int _WindowLeft;

        public int WindowLeft
        {
            get { return _WindowLeft; }
            set { _WindowLeft = value; RaisePropertyChanged("WindowLeft"); }
        }
        private int _WindowTop;

        public int WindowTop
        {
            get { return _WindowTop; }
            set { _WindowTop = value; RaisePropertyChanged("WindowTop"); }
        }



        public int BeginNumber
        {
            set
            {
                Begin = null;
                foreach (var node in Nodes)
                    if (node.Name == value.ToString())
                        Begin = node;
                RaisePropertyChanged("BeginNode.X");
                Message = "Wpisz punkt końcowy";
            }
        }
        public int EndNumber
        {
            set
            {
                End = null;
                foreach (var node in Nodes)
                    if (node.Name == value.ToString())
                        End = node;
                Message = "Wcisnij \"Znajdz najkrótszą ścieżkę\"";
            }
        }

        public GraphViewModel()
        {
            var conventer = new ColorConverter();
            LinearGradientBrush myLinearGradientBrush = new LinearGradientBrush();
            myLinearGradientBrush.StartPoint = new Point(0.5, 0);
            myLinearGradientBrush.EndPoint = new Point(0.5, 1);
            myLinearGradientBrush.GradientStops.Add(new GradientStop((Color)conventer.ConvertFromInvariantString("#FF003FFF"), 1.0));
            myLinearGradientBrush.GradientStops.Add(new GradientStop((Color)conventer.ConvertFromInvariantString("#FF37AFF9"), 0.0));

            RadialGradientBrush pointBrush = new RadialGradientBrush();
            pointBrush.Center = new Point(0.5, 0.5);
            pointBrush.GradientOrigin = new Point(0.5, 0.5);
            pointBrush.RadiusX = 0.5;
            pointBrush.RadiusY = 0.5;
            pointBrush.GradientStops.Add(new GradientStop((Color)conventer.ConvertFromInvariantString("#FFFF659D"), 0.0));
            pointBrush.GradientStops.Add(new GradientStop((Color)conventer.ConvertFromInvariantString("#FF930E2F"), 1.0));
            pointBrush.SpreadMethod = GradientSpreadMethod.Pad;

            Update();
            Message = "Wpisz ilość wierzchołków";
            ColorLow1 = pointBrush;
            ColorLow2 = myLinearGradientBrush;
            ColorHeight = Brushes.Yellow;
            ColorBackground = Brushes.White;
            rotateAngle = 0;
        }

        public void Update()
        {
            Nodes = new ObservableCollection<Node>(_graph.NodeList);
            Edges = new ObservableCollection<Edge>(_graph.EdgeList);
            if (ShortestPath != null) ShortestPathEdges = new ObservableCollection<Edge>(ShortestPath.EdgeList);
            RaisePropertyChanged("Nodes");
            RaisePropertyChanged("Edges");
            RaisePropertyChanged("ShortestPathEdges");
        }

       
        public Thread tt = null;

        private MediaState _pBehavior = MediaState.Stop;

        public MediaState pBehavior
        {
            get { return _pBehavior; }
            set { _pBehavior = value; RaisePropertyChanged("pBehavior"); }
        }
        private double _rotateAngle;

        public double rotateAngle
        {
            get { return _rotateAngle; }
            set { _rotateAngle = value; RaisePropertyChanged("rotateAngle"); }
        }

        private RelayCommand _SetEdges;

        public RelayCommand SetEdges
        {
            get
            {
                if (_SetEdges == null) _SetEdges = new RelayCommand(SetEdgesFunc);
                return _SetEdges;
            }
        }

        private RelayCommand _FindShortestPath;
        public ICommand FindShortestPath
        {
            get
            {
                if (_FindShortestPath == null) _FindShortestPath = new RelayCommand(FindShortestPathFunc);
                return _FindShortestPath;
            }
        }

        private RelayCommand _ClearShortestPath;
        public ICommand ClearShortestPath
        {
            get
            {
                if (_ClearShortestPath == null) _ClearShortestPath = new RelayCommand(ClearShortestPathFunc);
                return _ClearShortestPath;
            }
        }

        private RelayCommand _newGraph;
        public ICommand NewGraph
        {
            get
            {
                if (_newGraph == null)
                {
                    _newGraph = new RelayCommand(NewGraphFunc);
                }
                return _newGraph;
            }
        }

        private RelayCommand _loadGraph;
        public ICommand LoadGraph
        {
            get
            {
                if (_loadGraph == null)
                {
                    _loadGraph = new RelayCommand(LoadGraphFunc);
                }
                return _loadGraph;
            }
        }

        private RelayCommand _saveGraph;
        public ICommand SaveGraph
        {
            get
            {
                if (_saveGraph == null)
                {
                    _saveGraph = new RelayCommand(SaveGraphFunc);
                }
                return _saveGraph;
            }
        }

        private RelayCommand _generateGraph;
        public ICommand GenerateGraph
        {
            get
            {
                if (_generateGraph == null)
                {
                    _generateGraph = new RelayCommand(GenerateNewGraphFunc);
                }
                return _generateGraph;
            }
        }

        private void NewGraphFunc(object parameter)
        {
            _graph = new Graph();
            Update();
        }

        private void LoadGraphFunc(object parameter)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Plik w formacie xml | *.xml";

            if (ofd.ShowDialog() == false)
                return;

            IGraphReader xmlReader = new XMLReader();
            _graph = xmlReader.Read(ofd.FileName);
            IloscWierszy = _graph.NoOfRows;
            SetEdgesFunc(new object());
            Update();
            // TODO open
        }

        private void SaveGraphFunc(object parameter)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Plik w formacie xml | *.xml";

            if (sfd.ShowDialog() == false)
                return;


            IGraphWriter writer = new XMLWriter();
            writer.Write(sfd.FileName, _graph);

        }

        private void GenerateNewGraphFunc(object parameter)
        {
            if (IsCohesion)
            {
                _graph = GraphGenerator.GenerateCohesion(NumOfNodes, NumOfEdges, 50);
                IloscWierszy = _graph.NoOfRows;
            }
            else
            {
                _graph = GraphGenerator.Generate(NumOfNodes, NumOfEdges);
                IloscWierszy = _graph.NoOfRows;
            }
            SetEdgesFunc(new object());
            Update();
            Message = "Wpisz punkt startowy";
            // TODO generate
        }

        private void ClearShortestPathFunc(object obj)
        {
            ShortestPath = GraphPath.EmptyGraphPath;
        }

        public void FindShortestPathFunc(object obj)
        {
            Graph g = new Graph();
            foreach (var item in MyGraph.NodeList) g.NodeList.Add(item);
            foreach (var item in MyGraph.EdgeList)
            {
                g.EdgeList.Add(new Edge() { Begin = item.Begin, End = item.End, Name = item.Name, Weight = item.Weight });
                g.EdgeList.Add(new Edge() { Begin = item.End, End = item.Begin, Name = item.Name, Weight = item.Weight });
            }
            if (Begin != null && End != null)
            {
                ShortestPath = GraphPathSearcher.SearchPath(g, Begin, End);
                ShortestPathEdges = new ObservableCollection<Edge>(ShortestPath.EdgeList);
                RaisePropertyChanged("ShortestPathEdges");
                Message = "Baw się :)";
            }
            else
            {
                Message = "Błąd w początkowym lub końcowym wierzchołku";
            }
        }

        private void SetEdgesFunc(object obj)
        {
            var r = _windowWidth < _windowHeight ? _windowWidth / 3 : _windowHeight / 3;
            var xOffset = _windowWidth / 2 - PointSize / 2;
            var yOffset = (int)(_windowHeight / 2.5) - PointSize / 2;

            try
            {
                int wiersze = (int)(Math.Pow(IloscWierszy, 2)); // potegowanie do kwadratu
                GraphGenerator.DrawNet(ref _graph, IloscWierszy); // rysowanie siatki
                Update();
            }
            catch (Exception ex)
            {

            }

        }

        public void RaisePropertyChanged(string pName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(pName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    class RelayCommand : ICommand
    {
        Action<object> _operation;

        public RelayCommand(Action<object> operation)
        {
            _operation = operation;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            _operation(parameter);
        }


    }
}
