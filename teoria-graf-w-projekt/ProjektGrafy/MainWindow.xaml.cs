﻿using ProjektGrafy.Model;
using ProjektGrafy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjektGrafy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : RibbonWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        Node draging = null;
        private void Ellipse_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (draging == null)
                {
                    Ellipse ellipse = sender as Ellipse;
                    Node node = ellipse.DataContext as Node;
                    draging = node;
                }
                if (draging != null)
                {
                    GraphViewModel context = (this.DataContext as GraphViewModel);
                    double PointSize = context.PointSize;

                    double x = e.GetPosition(sender as Ellipse).X;
                    double y = e.GetPosition(sender as Ellipse).Y;
                    draging.X += x - (int)(PointSize / 2);
                    draging.Y += y - (int)(PointSize / 2);
                    context.Update();
                }
            }
            else
            {
                draging = null;
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (draging != null)
                {
                    GraphViewModel context = (this.DataContext as GraphViewModel);
                    double PointSize = context.PointSize;

                    double x = e.GetPosition(sender as Canvas).X;
                    double y = e.GetPosition(sender as Canvas).Y;
                    draging.X = x - (int)(PointSize / 2);
                    draging.Y = y - (int)(PointSize / 2);
                    context.Update();
                }
            }
            else
            {
                draging = null;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            GraphViewModel context = (this.DataContext as GraphViewModel);
            context.FindShortestPathFunc(new object());
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if ((this.DataContext as GraphViewModel).tt != null)
            {
                (this.DataContext as GraphViewModel).tt.Abort();
                (this.DataContext as GraphViewModel).pBehavior = MediaState.Stop;
                (this.DataContext as GraphViewModel).ColorLow1 = Brushes.Red;
                (this.DataContext as GraphViewModel).ColorLow2 = Brushes.Blue;
                (this.DataContext as GraphViewModel).ColorHeight = Brushes.Yellow;
                (this.DataContext as GraphViewModel).ColorBackground = Brushes.White;
                (this.DataContext as GraphViewModel).rotateAngle = 0;
                (this.DataContext as GraphViewModel).PointSize = 25;

            }
        }
    }
}

