﻿using ProjektGrafy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektGrafy.Model
{
    class GraphPath
    {
        public List<Edge> EdgeList { get; set; }
        public Node start { get; set; }
        public Node end { get; set; }

        public GraphPath()
        {
            this.EdgeList = new List<Edge>();
        }
        public Node GetChild(Node node)
        {
            return EdgeList.Where(n => n.Begin == node).First().End;
        }
        public override string ToString()
        {
            string result = "";
            foreach (var item in EdgeList)
            {
                result += "-> " + item;
            }
            return result;
        }


        static public GraphPath EmptyGraphPath
        {
            get
            {
                return new GraphPath();
            }
        }
    }
}
