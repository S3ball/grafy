﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ProjektGrafy.Model
{
    class XMLWriter : IGraphWriter
    {
        public void Write(string fileName, Graph graph)
        {
            XElement xElem = new XElement("Graph",
                new XElement("Nodes", from node in graph.NodeList
                                      select new XElement("Node", 
                                          new XElement("Name", node.Name))),
                new XElement("Edges",  from edge in graph.EdgeList
                                          select new XElement("Edge",
                                              new XElement("Begin", edge.Begin),
                                                 new XElement("End", edge.End),
                                                 new XElement("Name", edge.Name),
                                                 new XElement("Weight", edge.Weight))));

            try
            {
                xElem.Save(fileName);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(string.Format("Błąd zapisu{0}{1}", Environment.NewLine, ex.Data));
            }
        }
    }
}
