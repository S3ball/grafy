﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektGrafy.Model
{
    interface IGraphWriter
    {
         void Write(string fileName, Graph graph);
    }
}
