﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ProjektGrafy.Model
{
    class XMLReader : IGraphReader
    {
        static public int CountRows(int n)
        {
            int i = 0;
            while (true)
            {
                i++;

                if (n <= i * i)
                    return i;

            }
        }
        public Graph Read(string fileName)
        {
            XElement xElem = XElement.Load(fileName);
            Graph graph = new Graph();

            graph.EdgeList.Clear();
           
            graph.NodeList = (from node in xElem.Elements("Nodes").Elements("Node")
                              select new Node() { Name = (string)node.Element("Name") }).ToList();

            graph.EdgeList = (from edge in xElem.Elements("Edges").Elements("Edge")
                              select new Edge()
                              {
                                  Name = edge.Element("Name").ToString(),       
                                  Begin = graph.NodeList.Where(n=>n.Name == edge.Element("Begin").Value.ToString()).First() ,
                                  End = graph.NodeList.Where(n=>n.Name == edge.Element("End").Value.ToString()).First(),
                                  Weight = (double)edge.Element("Weight")
                              }).ToList();
            graph.NoOfRows = CountRows(graph.NodeList.Count);

            return graph;
        }
    }
}
