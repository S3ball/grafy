﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektGrafy.Model
{
    public class Edge
    {
        public Node Begin { get; set; }
        public Node End { get; set; }

        double _weight;
        public double Weight
        {
            get { return _weight; }
            set { _weight = value > 0 ? value : 1; }
        }

        const string _unnamed = "NaN";
        private string _name = _unnamed;
        public string Name
        {
            get
            {
                if (_name == _unnamed)
                    Name = string.Format("{0}_{1}", Begin, End);

                return _name;
            }
            set { _name = value; }
        }

        #region static

        public static bool operator ==(Edge a, Edge b)
        {
            try // zabezpieczenie przed nullami, jak wiecie jak to lepiej zrobic to to zmiencie
            {
                var tmp = a.Begin;
            }
            catch (Exception)
            {
                try
                {
                    var tmp = b.Begin;
                }
                catch (Exception)
                {
                    return true;
                }
                return false;
            }

            try
            {
                if ((a.Begin == b.Begin && a.End == b.End) ||
                       (a.Begin == b.End && a.End == b.Begin))
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public override bool Equals(object o)
        {
            if (o is Edge)
                return this == (o as Edge);
            else
                return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator !=(Edge a, Edge b)
        {
            return !(a == b);
        }
        public override string ToString()
        {
            return Name;
        }
        #endregion
    }
}
