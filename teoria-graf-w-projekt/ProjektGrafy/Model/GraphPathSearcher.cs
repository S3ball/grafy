﻿using ProjektGrafy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektGrafy.Model
{
    class GraphPathSearcher
    {
        static public GraphPath SearchPath(Graph graph, Node start, Node end)
        {
            GraphPath result = new GraphPath() { start = start, end = end }; // to nie tak
            List<GraphPath> open_list = GetChildrenPath(start, graph.EdgeList);
            List<GraphPath> close_list = new List<GraphPath>();
            while (true)
            {
                List<GraphPath> sorted = open_list.OrderBy(n => GetSum(n)).ToList();
                result = sorted[0];
                open_list.Remove(result);
                close_list.Add(result);
                if (result.end == end)
                {
                    return result;
                }
                foreach (var item in GetChildrenPath(result.end, graph.EdgeList))
                {
                    GraphPath tmp = new GraphPath() { start = start, end = item.end };
                    tmp.EdgeList = result.EdgeList.Concat(item.EdgeList).ToList();
                    open_list.Add(tmp);
                }
            }

           // return result;
        }


        #region algorithm

        static private List<Node> GetChildrenNode(Node node, List<Edge> EdgeList)
        {
            List<Node> children = new List<Node>();
            foreach (var item in EdgeList.Where(n => n.Begin == node))
                children.Add(item.End);
            return children;
        }
        static private List<GraphPath> GetChildrenPath(Node node, List<Edge> EdgeList)
        {
            List<GraphPath> children = new List<GraphPath>();
            foreach (var item in EdgeList.Where(n => n.Begin == node))
            {
                GraphPath tmp = new GraphPath() { start = node, end = item.End };
                tmp.EdgeList.Add(item);
                children.Add(tmp);
            }
            return children;
        }
        static private double GetSum(GraphPath path)
        {
            return path.EdgeList.Sum(n => n.Weight);
        }
        #endregion
    }
}
