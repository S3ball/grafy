﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektGrafy.Model
{
    interface IGraphReader
    {
       Graph Read(string fileName);
    }
}
