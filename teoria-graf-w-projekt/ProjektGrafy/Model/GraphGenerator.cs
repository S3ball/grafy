﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektGrafy.Model
{
    static class GraphGenerator
    {
        static public int CountRows(int n)
        {
            int i = 0;
            while (true)
            {
                i++;

                if (n <= i * i)
                    return i;
                
            }
        }
        static public Graph Generate(int n, int k, int x = 0, int y = 10, int r = 10)
        {
            var graph = new Graph();

            for (int i = 0; i < n; i++)
            {
                graph.NodeList.Add(new Node() { X = r * Math.Sin(2.0 * Math.PI * i / n) + x, Y = r * Math.Cos(2.0 * Math.PI * i / n) + y, Name = i.ToString() });
            }
            Generate(k, ref graph);
            graph.NoOfRows = CountRows(n);
            return graph;
        }

        static public void GenerateOLD(int k, ref Graph graph)
        {
            if (graph.NodeList.Count <= 1)
                return;

            Random rand = new Random();
            graph.EdgeList.Clear();
            for (int i = 0; i < k; i++)
            {
                Edge node = new Edge();
                node.Begin = graph.NodeList[rand.Next(graph.NodeList.Count)];
                node.End = graph.NodeList[rand.Next(graph.NodeList.Count)];
                node.Name = string.Format("{0} - {1}", node.Begin.Name, node.End.Name);

                if (node.Begin == node.End || graph.EdgeList.Where(n => n == node).FirstOrDefault() != null)
                {
                    i--;
                    continue;
                }

                graph.EdgeList.Add(node);
            }
        }

        static public void Generate(int k, ref Graph graph)
        {
            if (graph.NodeList.Count <= 1)
                return;

            graph.EdgeList.Clear();
            GenerateAll(ref graph);


            Random rand = new Random();
            while (graph.EdgeList.Count > k)
            {
                graph.EdgeList.Remove(graph.EdgeList[rand.Next(graph.EdgeList.Count)]);
            }
        }

        static public Graph GenerateCohesion(int n, int k, int r = 10)
        {
            var graph = new Graph();
            for (int i = 0; i < n; i++)
            {
                graph.NodeList.Add(new Node() { X = r * Math.Sin(2.0 * Math.PI * i / n), Y = r * Math.Cos(2.0 * Math.PI * i / n), Name = i.ToString() });
            }
            GenerateCohesion(k, ref graph);
            graph.NoOfRows = CountRows(n);
            return graph;
        }

        static public void GenerateCohesion(int k, ref Graph graph)
        {
            if (graph.NodeList.Count - k > 1)
                return;

            graph.EdgeList.Clear();
            GenerateAll(ref graph);

            Random rand = new Random();
            while (graph.EdgeList.Count > k)
            {
                var tmp = graph.EdgeList[rand.Next(graph.EdgeList.Count)];
                graph.EdgeList.Remove(tmp);
                if (!graph.CheckCohesion())
                {
                    graph.EdgeList.Add(tmp);
                }
            }
        }

        static public void GenerateAll(ref Graph graph)
        {
            graph.EdgeList.Clear();
            Random rand = new Random();
            for (int i = 0; i < graph.NodeList.Count; i++)
            {
                for (int j = i + 1; j < graph.NodeList.Count; j++)
                {
                    Edge node = new Edge();
                    node.Begin = graph.NodeList[i];
                    node.End = graph.NodeList[j];
                    node.Name = string.Format("{0} - {1}", node.Begin.Name, node.End.Name);
                    node.Weight = rand.Next(100);
                    graph.EdgeList.Add(node);
                }
            }
        }

        static public void DrawNet(ref Graph graph, int ilWierszy)
        {
            int przesuniecie = 160;
            var n = graph.NodeList.Count;
            int counter = 0;
            Random rnd = new Random();
            for (int i = 0; i < n / ilWierszy + 1; i++)
            {

                for (int j = 0; j < ilWierszy; j++)
                {
                    if (counter < n)
                    {
                        graph.NodeList[i * ilWierszy + j].X = i * (przesuniecie + rnd.Next(12, 35));
                        graph.NodeList[i * ilWierszy + j].Y = j * (przesuniecie + rnd.Next(12, 35));
                        counter = counter + 1;
                    }
                }
            }
        }

        // Rysuj wiezcholki grafu na kole
        static public void DrawCircle(ref Graph graph, int radius)
        {
            
        }
    }
}
