﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektGrafy.Model
{
    public class Graph
    {
        public List<Node> NodeList { get; set; }
        public List<Edge> EdgeList { get; set; }
        public int NoOfRows { get; set; }

        public Graph()
        {
            NodeList = new List<Node>();
            EdgeList = new List<Edge>();
        }

        public bool CheckCohesion()
        {
            for (int i = 1; i < this.NodeList.Count; i++)
            {
                if (!CheckWay(0, i))
                {
                    return false;
                }
            }

            return true;
            //TODO
        }

        public bool CheckWay(int from, int to)
        {
            if (from >= NodeList.Count || to >= NodeList.Count)
                return false;

            List<Node> nodes = this.NodeList;
            List<Node> open_list = new List<Node>();
            List<Node> closed_list = new List<Node>();

            open_list.Add(nodes[0]);

            do
            {
                if (open_list.Count == 0)
                    return false;
                Node node = open_list[0];

                if (this.NodeList[to] == node)
                    return true;

                open_list.Remove(node);

                closed_list.Add(node);

                List<Node> tmp = new List<Node>();
                foreach (var item in this.EdgeList)
                {
                    if (item.Begin == node)
                        open_list.Add(item.End);
                    else if (item.End == node)
                        open_list.Add(item.Begin);
                }

                foreach (var item in closed_list)
                {
                    open_list.Remove(item);
                }

            } while (true);
        }
    }
}
